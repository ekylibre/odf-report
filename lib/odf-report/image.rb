module ODFReport
  class Image < Field

    IMAGE_DIR_NAME = "Pictures"

    attr_reader :files

    def initialize(opts, &block)
      @files = []
      @keep_ratio = opts[:keep_ratio] || true
      super
    end

    def replace!(doc, data_item = nil)

      frame = doc.xpath("//draw:frame[@draw:name='#{@name}']").first
      image = doc.xpath("//draw:frame[@draw:name='#{@name}']/draw:image").first

      return unless image

      puts image.parent.inspect.red

      update_node_ratio(image.parent) if @keep_ratio

      file = @data_source.value

      if file
        image.attribute('href').content = File.join(IMAGE_DIR_NAME, File.basename(file))
        frame.attribute('name').content = SecureRandom.uuid

        @files << file
      else
        frame.remove
      end

    end

    def self.include_image_file(zip_file, image_file)
      return unless image_file

      href = File.join(IMAGE_DIR_NAME, File.basename(image_file))

      zip_file.update_file(href, File.read(image_file))
    end

    def self.include_manifest_entry(content, image_file)
      return unless image_file

      return unless root_node = content.at("//manifest:manifest")

      href = File.join(IMAGE_DIR_NAME, File.basename(image_file))

      entry = content.create_element('manifest:file-entry')
      entry['manifest:full-path']  = href
      entry['manifest:media-type'] = MIME::Types.type_for(href)[0].content_type

      root_node.add_child entry

    end

    def compute_ratio
      path = File.join(IMAGE_DIR_NAME, File.basename(@data_source.value))
      width, height = FastImage.size(path)

      width.to_f / height.to_f
    end

    def update_node_ratio(node)
      node_width, node_height = get_node_image_dimensions(node)
      return if node_width.nil? || node_height.nil?
      node_ratio = node_width / node_height
      image_ratio = compute_ratio

      if image_ratio < node_ratio # Image is narrower than target
        update_node_size_attribute(node, :width, node_width * image_ratio / node_ratio)
      elsif image_ratio > node_ratio # Image is wider than target
        update_node_size_attribute(node, :height, node_height * node_ratio / image_ratio)
      end
    end

    def update_node_size_attribute(node, attribute, value)
      node.attribute(attribute.to_s).value = "#{value}cm"
    end

    def get_node_image_dimensions(node)
      if node.attribute('width').present?
        width_attr = 'width'
        height_attr = 'height'
      elsif node.attribute('svg:width').present?
        width_attr = 'svg:width'
        height_attr = 'svg:height'
      end
      node_width = extract_centimeters node.attribute(width_attr).value
      node_height = extract_centimeters node.attribute(height_attr).value

      [node_width, node_height]
    end

    def extract_centimeters(value)
      value.match(/\A(\d*\.?\d+)cm\z/) { |m| m[1].to_f }
    rescue
      nil
    end

  end
end
